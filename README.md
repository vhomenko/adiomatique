Adiomatique
===========

A __specialized__ WordPress plugin, that builds upon categories and custom fields of posts and pages, to provide some very basic event management for the [Autodidaktische Initiative](http://adi-leipzig.net).

Requirements
------------
[jQuery UI Timepicker by François Gélinas](https://github.com/fgelinas/timepicker)

License
-------
[GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.html)
