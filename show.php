<?php

namespace adi;

defined( 'ABSPATH' ) or die( '' );


add_filter( 'the_content', 'adi\add_event_data', 20 );

add_shortcode( 'adi_termine', 'adi\the_events' );

function add_event_data( $content ) {
	$id = get_the_ID();
	$titlepage_cat_id = intval( get_post_meta( $id, 'adi_titlepage_cat_id', true ) );

	/* ODOT same integrity check as in admin_page */

	$data = '';
	if ( 0 !== $titlepage_cat_id ) {
		$data = display_page( $titlepage_cat_id );
	} else {
		$data = display_post( $id );
	}
	return $data . $content;
}


function display_page( $titlepage_cat_id ) {
	global $post;
	if ( ACTIVITY_ARCHIVE_PAGE_ID === $post->post_parent ) {
		return '<span class="small_right">Dieses Projekt ist archiviert.</span>';
	}
	$events = new EventManager( $titlepage_cat_id );
	if ( $events->isEmpty() ) {
		return;
	}

	$output = '<p>';
	foreach ( $events as $e ) {
		$output .= $e->getDate() . ', ';
		$output .= $e->getTime() . ' Uhr &nbsp;&nbsp;&nbsp;' . $e->getLink() . ' &nbsp;&nbsp;&nbsp;';

		if ( $e->isExternal() ) 
			$output .= '<i>extern</i>';
		else if ( $e->isPeriodic() ) 
			$output .= '<i>regelmäßig</i>';

		$output .= '<br>';
	}
	$output .= '</p>';
	return '<span>Anstehende Termine:<a class="small_right" href="' . get_category_link( $titlepage_cat_id ) . '">Alle Termine ansehen</a><br>' . $output . '</span>';
}


function display_post( $id ) {
	$e = new Event( $id );
	if ( $e->isEmpty() ) return;

	$link = $e->getTitlepageLink();
	$parent_link = ' <span class="small_right">' . $link . '</span>';
	if ( empty( $link ) ) $parent_link = '';

	$location = '<br><strong>Ort:</strong> ' . $e->getLocation() . '';
	if ( ! $e->isExternal() ) $location = '';

	$type = '';
	if ( $e->isArchived() ) {
		$type = 'Archivierter ';
	}

	return
		'<p><strong>' . $type . 'Termin:</strong> ' .
		'am ' . $e->getDate() . ' um ' . $e->getTime() . ' Uhr' . '<i>' . $e->getPeriodicityDesc() . '</i>.' . $location .
		$parent_link . '</p>';

}


function the_events( $atts ) {
	wp_enqueue_style( 'adiomatique-css', plugins_url() . '/adiomatique/css/adiomatique.css' );

	$a = shortcode_atts( array(
		'zeige_tage' => '8',
	), $atts );

	if ( 'alle' === $a['zeige_tage'] ) {
		$output_limit = false;
	} else {
		$output_limit = intval( $a['zeige_tage'] );
	}

	$output = "<div id='aditue'>";

	$events = new EventManager( EVENTS_CAT_ID, $output_limit );

	$prevE = null;

	foreach ( $events as $e ) {

		$output .= '<div>';

		if ( ! $e->isOnTheSameDay( $prevE ) ) {
			$output .= "<div class='adi_date'><span>" . $e->getDate() . "</span><span>" . $e->getWeekday() . "</span></div>";
		}

		$output .= "<div class='adi_time'><span>" . $e->getTime() . "</span><span>" . $e->getLink() . "</span></div>";

		$type = '';
		$type_fname = '';

		if ( $e->isExternal() ) {
			$type = "extern";
			$type_fname = 'external.png';
		} else if ( $e->isPeriodic() ) {
			$type = "regelmäßig";
			$type_fname = 'periodic.png';
		}

		$type_template = '';

		if ( ! empty( $type ) ) {
			$type_template = "<img class='" . ( '' !== $type ? 'adi_type_img' : '') . "' src='" . plugins_url() . "/adiomatique/images/" . $type_fname . "' alt='" . $type . "'>";
		}

		$periodicity = $e->getPeriodicityDesc();
		$periodicity_click = " onclick=\"javascript:alert(' " . $e->getTitle() . ":\\n" . $periodicity . "')\"";

		if ( ! $e->isPeriodic() ) {
			$periodicity_click = '';
		}

		$link = $e->getTitlepageLink();

		$sub = "<div class='adi_type'><span " . ( ! $e->isPeriodic() ? "" : "style='cursor:pointer;'" ) . $periodicity_click . ">" . $type_template . "</span><span>" . $link . "</span>";

		$sub .= "</div>";

		if ( ! empty( $type ) || ! empty( $link ) ) {
			$output .= $sub;
		}

		$output .= '</div>';

		$prevE = $e;
	}

	$output .= '</div><!--/aditue-->';

	return $output;
}
