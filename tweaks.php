<?php

namespace adi;

if( is_admin() ) {

	if ( $_SERVER["SERVER_ADDR"] !== '127.0.0.1' ) {
		add_filter( 'hidden_meta_boxes', 'adi\hide_meta_boxes', 10, 2 );
	}
	function hide_meta_boxes( $hidden, $screen ) {
		global $post;

		if ( empty( $post ) ) return $hidden;

		$adi_is_titlepage = get_post_meta( $post->ID, 'adi_is_titlepage', true );
		$adi_event_timestamp = get_post_meta( $post->ID, 'adi_event_timestamp', true );

		if ( 'post' == $screen->post_type && ! empty( $adi_event_timestamp ) ) {
			$hidden = array( 'postexcerpt', 'slugdiv', 'trackbacksdiv', 'commentstatusdiv', 'commentsdiv', 'authordiv', 'revisionsdiv', 'categorydiv', 'postcustom' );
		} else if ( 'page' == $screen->post_type && ! empty( $adi_is_titlepage ) ) {
			$hidden = array( 'postexcerpt', 'slugdiv', 'trackbacksdiv', 'commentstatusdiv', 'commentsdiv', 'authordiv', 'revisionsdiv', 'pageparentdiv', 'postcustom' );
		}

		return $hidden;
	}

	if ( $_SERVER["SERVER_ADDR"] === '127.0.0.1' ) {
		add_action( 'admin_print_scripts', 'adi\dequeue_autosave_script', 100 );
	}
	function dequeue_autosave_script() {
		wp_dequeue_script( 'autosave' );
	}

} else {

	add_filter( 'the_generator', 'adi\remove_wp_version' );
	function remove_wp_version() {
		return '';
	}


	/* in case the default category for posts was changed, serve only that */
	add_filter( 'pre_get_posts', 'adi\filter_rss_query' );
	function filter_rss_query( $query ) {
		if ( $query->is_feed ) {
			$query->set( 'cat', get_option( 'default_category' ) );
		}

		return $query;
	}

}
